//
//  Persistance.swift
//  Weather
//
//  Created by Александр Осипов on 22.02.2020.
//  Copyright © 2020 Александр Осипов. All rights reserved.
//

import Foundation
import RealmSwift

class PersistanceRealm {
    static var shared = PersistanceRealm()
    private let realm = try! Realm()
    
    func load(_ forecast: Forecast) {
        try! realm.write {
            realm.add(forecast)
        }
    }
    
    func download() -> [Forecast] {
        return realm.objects(Forecast.self).array
    }
    
    func delete() {
        try! realm.write {
            realm.deleteAll()
        }
    }
}

extension Results {
    var array: [Element] {
        return self.count > 0 ? self.map { $0 } : []
    }
}
