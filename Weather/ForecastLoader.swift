//
//  ForecastLoader.swift
//  Weather
//
//  Created by Александр Осипов on 19.01.2020.
//  Copyright © 2020 Александр Осипов. All rights reserved.
//

import Foundation
import RealmSwift

protocol ForecastLoaderDelegate {
    func loaded(forecasts: [Forecast])
}

class ForecastLoader {
    
    var delegate: ForecastLoaderDelegate?
    
    var forecasts: [Forecast] = []
    
    func loadСurrentForcast() {
        let url = URL(string: "https://api.openweathermap.org/data/2.5/weather?q=Kirov&units=metric&appid=657f61591f30478935c6e1a2af963529")!
        let request = URLRequest(url: url)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if let data = data,
                let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments),
                let jsonDict = json as? NSDictionary {
                var forecastData: [String: String] = [:]
                for str in jsonDict {
                    if str.key as! String == "main" {
                        forecastData["temp"] = (((str.value as! NSDictionary)["temp"]!) as! NSNumber).stringValue
                    } else if str.key as! String == "weather" {
                        forecastData["description"] = (((str.value as! NSArray)[0] as! NSDictionary)["description"] as! String)
                        forecastData["icon"] = (((str.value as! NSArray)[0] as! NSDictionary)["icon"] as! String)
                    }
                }
                forecastData["dt"] = ""
                if let forecast = Forecast(data: forecastData) {
                    self.forecasts.append(forecast)
                }
                print(self.forecasts.count)
                DispatchQueue.main.async {
                    self.delegate?.loaded(forecasts: self.forecasts)
                }
            }
        }
        task.resume()
    }
    
    func loadForecast5day() {
        let url = URL(string: "https://api.openweathermap.org/data/2.5/forecast?q=Kirov&units=metric&appid=657f61591f30478935c6e1a2af963529")!
        let request = URLRequest(url: url)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if let data = data,
                let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments),
                let jsonDict = json as? NSDictionary {
                print(type(of: json))
//                var forecasts: [Forecast] = []
                for lvl1 in jsonDict { //where data is NSDictionary {
                    if lvl1.key as! String == "list" {
                        for lvl2 in lvl1.value as! NSArray { //where data is NSDictionary {
                            var forecastData: [String: String] = [:] //["dt": "", "icon": "", "description": "", "temp": ""]
                            for lvl3 in lvl2 as! NSDictionary {
                                if lvl3.key as! String == "dt" {
                                    forecastData["dt"] = String(lvl3.value as! Int)
                                } else if lvl3.key as! String == "main" {
                                    forecastData["temp"] = (((lvl3.value as! NSDictionary)["temp"]!) as! NSNumber).stringValue
                                } else if lvl3.key as! String == "weather" {
                                    forecastData["description"] = (((lvl3.value as! NSArray)[0] as! NSDictionary)["description"] as! String)
                                    forecastData["icon"] = (((lvl3.value as! NSArray)[0] as! NSDictionary)["icon"] as! String)
                                }
                            }
                            if let forecast = Forecast(data: forecastData) {
                                self.forecasts.append(forecast)
                            }
                        }
                    }
                }
                print(self.forecasts.count)
                DispatchQueue.main.async {
                    self.delegate?.loaded(forecasts: self.forecasts)
                }
            }
        }
        task.resume()
        
    }
}
