//
//  NewViewController.swift
//  Weather
//
//  Created by Александр Осипов on 19.01.2020.
//  Copyright © 2020 Александр Осипов. All rights reserved.
//

import UIKit

class NewViewController: UIViewController {
    
    @IBOutlet weak var tableViev: UITableView!
    var forecasts: [Forecast] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        forecasts = PersistanceRealm.shared.download()
        let loader = ForecastLoaderAlamofire()
        loader.delegate = self
        loader.loadForcast()
    }
}

extension NewViewController: ForecastLoaderAlamofireDelegate {
    func loaded(forecasts: [Forecast]) {
        self.forecasts = forecasts
        tableViev.reloadData()
    }
}

extension NewViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return forecasts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewCell", for: indexPath) as! NewCell
        let object = forecasts[indexPath.row]
        cell.date.text = object.dt
        cell.weatherImage.image = UIImage(data: object.image)
        cell.weatherDescription.text = object.descriptionForecast
        cell.temperature.text = String((object.temp as NSString).integerValue)
        return cell
    }
}

class NewCell: UITableViewCell {
    
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var weatherImage: UIImageView!
    @IBOutlet weak var weatherDescription: UILabel!
    @IBOutlet weak var temperature: UILabel!
    
}

