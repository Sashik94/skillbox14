//
//  Forecast.swift
//  Weather
//
//  Created by Александр Осипов on 19.01.2020.
//  Copyright © 2020 Александр Осипов. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class Forecast: Object {
    
    @objc dynamic var dt: String
    @objc dynamic var image: Data
    @objc dynamic var descriptionForecast: String
    @objc dynamic var temp: String
    
    init?(data: [String: String]) {
        guard let dt = data["dt"],
            let icon = try? Data(contentsOf: URL(string: "http://openweathermap.org/img/wn/\(data["icon"]!)@2x.png")!),
            let descriptionForecast = data["description"],
            let temp = data["temp"] else {
                return nil
        }
        
        func date24(_ date: String) -> String {
            if !dt.isEmpty {
                let date = Date(timeIntervalSince1970: TimeInterval(dt)!)
                let dateFormatter = DateFormatter()
                dateFormatter.locale = NSLocale(localeIdentifier: NSLocale.system.identifier) as Locale?
                dateFormatter.dateFormat = "hh:mm a dd-MM-yyyy"
                let dateAsString = dateFormatter.string(from: date as Date)
                dateFormatter.dateFormat = "h:mm a dd/MM/yyyy"
                let date2 = dateFormatter.date(from: dateAsString)
                dateFormatter.dateFormat = "HH:mm dd/MM/yyyy"
                return dateFormatter.string(from: date2!)
            } else {
                return "Сейчас"
            }
        }
        
        self.dt = date24(dt)
        self.image = icon
        self.descriptionForecast = descriptionForecast
        self.temp = temp
    }
    
    required init() {
        self.dt = ""
        self.image = Data.init()
        self.descriptionForecast = ""
        self.temp = ""
    }
}
