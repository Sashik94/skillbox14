//
//  ForecastLoaderAlamofire.swift
//  Weather
//
//  Created by Александр Осипов on 19.01.2020.
//  Copyright © 2020 Александр Осипов. All rights reserved.
//

import Foundation
import Alamofire

protocol ForecastLoaderAlamofireDelegate {
    func loaded(forecasts: [Forecast])
}

class ForecastLoaderAlamofire {
    
    var delegate: ForecastLoaderAlamofireDelegate?
    
    var forecasts: [Forecast] = []
    
    func loadForcast() {
        AF.request("https://api.openweathermap.org/data/2.5/weather?q=Kirov&units=metric&appid=657f61591f30478935c6e1a2af963529").responseJSON { responseJSON in
            if let objects = responseJSON.value,
                let jsonDict = objects as? NSDictionary {
                var forecastData: [String: String] = [:]
                for str in jsonDict {
                    if str.key as! String == "main" {
                        forecastData["temp"] = (((str.value as! NSDictionary)["temp"]!) as! NSNumber).stringValue
                    } else if str.key as! String == "weather" {
                        forecastData["description"] = (((str.value as! NSArray)[0] as! NSDictionary)["description"] as! String)
                        forecastData["icon"] = (((str.value as! NSArray)[0] as! NSDictionary)["icon"] as! String)
                    }
                }
                forecastData["dt"] = ""
                if let forecast = Forecast(data: forecastData) {
                    self.forecasts.append(forecast)
                    PersistanceRealm.shared.delete()
                    PersistanceRealm.shared.load(forecast)
                }
            }
        }
        AF.request("https://api.openweathermap.org/data/2.5/forecast?q=Kirov&units=metric&appid=657f61591f30478935c6e1a2af963529").responseJSON { responseJSON in
            if let objects = responseJSON.value,
                let jsonDict = objects as? NSDictionary {
                for lvl1 in jsonDict { //where data is NSDictionary {
                    if lvl1.key as! String == "list" {
                        for lvl2 in lvl1.value as! NSArray { //where data is NSDictionary {
                            var forecastData: [String: String] = [:] //["dt": "", "icon": "", "description": "", "temp": ""]
                            for lvl3 in lvl2 as! NSDictionary {
                                if lvl3.key as! String == "dt" {
                                    forecastData["dt"] = String(lvl3.value as! Int)
                                } else if lvl3.key as! String == "main" {
                                    forecastData["temp"] = (((lvl3.value as! NSDictionary)["temp"]!) as! NSNumber).stringValue
                                } else if lvl3.key as! String == "weather" {
                                    forecastData["description"] = (((lvl3.value as! NSArray)[0] as! NSDictionary)["description"] as! String)
                                    forecastData["icon"] = (((lvl3.value as! NSArray)[0] as! NSDictionary)["icon"] as! String)
                                }
                            }
                            if let forecast = Forecast(data: forecastData) {
                                self.forecasts.append(forecast)
                                PersistanceRealm.shared.load(forecast)
                            }
                        }
                    }
                }
                print(self.forecasts.count)
                DispatchQueue.main.async {
                    self.delegate?.loaded(forecasts: self.forecasts)
                }
            }
        }
    }
}
